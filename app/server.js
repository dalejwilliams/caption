/*
CAPTION: A game by Dale J Williams
*/
'use strict';

const CaptionServer = require("./server/CaptionServer");
const WebServer = require("./server/WebServer");

const captionServer = new CaptionServer(26666);
const webServer = new WebServer(80);
