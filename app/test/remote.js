'use strict';

const http = require("http");
const punch = require("holepunch");
const network = require("network");

const PORT = 16666;
const PORT_PRIVATE = 16665;
const LISTEN_IP = "0.0.0.0";

class RemoteTest {

    constructor() {
        this.connect();
    }

    connect() {
        this.openGateway(this.startServer());
    }

    openGateway(cb) {
        network.get_gateway_ip(function(err, ip) {
            if (err) throw err;
            this.gateway = ip;
            console.log ("Gateway is " + this.gateway);

            network.get_public_ip(function(err, ip) {
                if (err) throw err;

                this.address = ip;
                console.log("IP is " + this.address);

                this.openPorts(cb);

            }.bind(this));

        }.bind(this));
    }

    openPorts(cb) {
        punch({
            debug: false,
            mappings: [{ internal: PORT, external: PORT, secure: true }],
            ipifyUrls: ['api.ipify.org'],
            protocols: ['none', 'upnp', 'pmp'],
            ifaces: ['none', 'upnp', 'pmp']
        })
        .then(
            function (mappings) {
                // be sure to check for an `error` attribute on each mapping 
                console.log(mappings);

                cb();

            }, 
            function (err) {
                console.log(err);
            }
        );
    }

    startServer() {
        this.httpServer = http.createServer(function(request, result) {
            response.writeHead(200, {"Content-Type": "text/plain"});
            response.end("Hello World\n");
        }.bind(this));

        this.httpServer.listen(PORT, LISTEN_IP);
        console.log("Server running at on " + LISTEN_IP + ":" + PORT);
    }

}

new RemoteTest();