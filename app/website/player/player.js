// (c) copyright 2016 Dale J Williams
(function() {

    'use strict';

    var player = window.player = {
        init: init,
        connect: connect,
        allHere: allHere,
        submit: submit,
        submitAnswer: submitAnswer,
        nextQuestion: nextQuestion,
        pickVote: pickVote,
        onConnect: onConnect,
        onMessage: onMessage
    };

    var data = null;

    var handlers = {
        "auth":         onAuth,
        "game-start":   onGameStart,
        "round-start":  onRoundStart,
        "round-reveal": onRoundReveal,
        "vote-start":   onVoteStart,
        "vote":         onVote,
        "results":      onResults,
        "error":        onError,
        "players":      onPlayers
    };

    function init() {
        app.init();
        app.goto("start");
        sock.handler = player;
    }

    function connect(data) {
        if (data.room) {
            data.room = data.room.toUpperCase();
        }
        else {
            onError({
                error: "Enter a room code",
                info: "Enter a room code to connect to the game"
            })
            return;
        }

        if (data.name) { 
            data.name = data.name.toUpperCase();
        }
        else {
            onError({
                error: "Enter your name",
                info: "Enter your name to connect to the game"
            })
            return;
        }

        app.save({
            lastRoom: data.room,
            lastName: data.name
        });

        if (window.location.href.indexOf("dwilderbeest.com") > 0) {
            connectRemote(data);
        }
        else {
            connectLocal(data); 
        }
    }

    function connectLocal(data) {
        var host = window.location.href;

        var httpI = host.indexOf("://");
        host = host.substr(httpI + 3);
        
        var slashI = host.indexOf("/");
        if (slashI > 0) {
            host = host.substr(0, slashI);
        }

        app.data.host = host;

        player.auth = data;
        sock.connect();
    }

    function connectRemote(data) {
        var url = "/run.php?action=find&room=" + data.room;
        utils.loadJson(url, null, function(result) {
            console.log(result);

            if (result == null) {
                onError({
                    error: "Couldn't connect.",
                    info: "Couldn't connect. Check your internet connection and try again."
                })
                return;
            }

            if (result.result == null) {
                onError({
                    error: "Couldn't find room.",
                    info: "No room by the name " + data.room + " could be found."
                })
                return;
            }

            app.data.host = result.result.address;

            player.auth = data;
            sock.connect();
        });       
    }

    function allHere() {
        sock.send("start-game", {
            player: app.data.playerIndex
        });
    }

    function onConnect() {
        sock.send("player-auth", player.auth);
    }

    function onMessage(type, data) {
        console.log("player message", type, data);

        var handler = handlers[type];
        if (handler != null) {
            handler(data);
        }
        else {
            console.log("No handler for " + type);
        }
    }

    function onAuth(msg) {
        app.data.room = msg.room;
        app.data.serverIP = msg.serverIP;
        app.data.playerIndex = msg.playerIndex;
        app.goto("joined");
        onPlayers(msg);
    }

    function onPlayers(msg) {
        app.data.playerCount = msg.players.length;
        utils.fillClasses("playerCount", app.data.playerCount);

        if (app.data.playerCount >= 2) {
            setTimeout(function() {
                var startButton = document.getElementById("start-button");
                startButton.removeAttribute("disabled");
            }, 100);
        }
    }

    function onGameStart(msg) {
        app.data.round = msg;
        app.goto("game-start");
    }

    function onRoundStart(msg) {
        app.data.round = msg;
        app.goto("round-start");
    }

    function onRoundReveal(msg) {
        app.data.roundData = msg;
        app.data.questionI = -1;
        app.data.answers = [];
        nextQuestion();
    }

    function submitAnswer(formData) {
        var question = app.data.roundData[app.data.questionI];
        formData.index = question.index;
        sock.send("answer", formData);
        nextQuestion();
    }

    function nextQuestion() {
        app.data.questionI++;
        if (app.data.questionI >= app.data.roundData.length) {
            endRound();
            return;
        }
        app.data.question = app.data.roundData[app.data.questionI];
        app.goto("round");
    }

    function endRound() {
        app.goto("round-wait");
    }

    function onVoteStart(msg) {
        app.goto("vote-start");
    }

    function onVote(msg) {
        app.data.vote = msg;
        app.goto("vote");
    }

    function pickVote(vote) {
        var msg = {
            index: app.data.vote.index,
            vote: vote
        };

        var buttons = document.getElementsByClassName("vote-pick");
        for (var i=0; i<buttons.length; i++) {
            var button = buttons[i];
            var newClass = i == vote - 1 ? "picked" : "unpicked";
            button.classList.add(newClass);
            button.setAttribute("disabled", "true");
        }

        sock.send("vote", msg);
    }

    function onVoted(msg) {
        app.goto("vote-tally");
    }

    function onResults(msg) {
        app.goto("results");
    }

    function onError(msg) {
        app.data.error = msg.error;
        app.data.info = msg.info;
        app.goto("error");
    }

    function submit(data) {
        console.log(data);
    }

    window.onload = function() {
        setTimeout(init, 1000);
    };

})();
