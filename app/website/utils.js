// Caption: (c) 2016-2017 copyright 2015 Dale J Williams
(function() {
	'use strict';

	var utils = window.utils = {
		friendlyName: friendlyName,
		titlize: titlize,
		initials: initials,

		mixin: mixin,
		mixinDeep: mixinDeep,
		clone: clone,
		cloneDeep: clone,
		childOfParent: childOfParent,
		getPosition: getPosition,
		removeSelection: removeSelection,
		goto: goto,

		isArray: isArray,
		lookup: lookup,
		inject: inject,
		filterDeep: filterDeep,
		loadJson: loadJson,
		loadString: loadString,
		loadHtml: loadHtml,
		jsonToObj: jsonToObj,
		objToJson: objToJson,
		stringToHue: stringToHue,
		warn: warn,
		formData: formData,
		latchForms: latchForms,
		fillClasses: fillClasses,
		contentPath: contentPath
	};

	Math.seed = function(s) {
	    return function() {
	        s = Math.sin(s) * 10000; return s - Math.floor(s);
	    };
	};

	function stringToHue(name) {
		if (!name) {
			return 0;
		}

		if (typeof name !== "string") {
			console.error("strignToHue name was not string", name);
			return 0;
		}

		name = name.toUpperCase();

		var seed = 0.0;
		for (var i=0; i < name.length; i++) {
			var code = name.charCodeAt(i);
			if (code < 65 || code > 90)
				continue;

			var inc = (code - 64) / 26;
			seed += inc;
		}

		seed = (seed - Math.floor(seed));
		var hue = Math.floor(Math.sin(seed) * 360);
		return hue;
	}

	function friendlyName(element) {
		if (element == null)
			return "null";

		var name = element.tagName.toLowerCase();
		if (element.id)
			name += "#" + element.id;

		if (element.className)
			name += "." + element.className.replace(" ", ".");

		return name;
	}

	function titlize(str) {
		str = str.replace("_", " ");
		str = str.replace("/", " ");

		var result = "";
		var words = str.split(" ");
		for (var i=0; i<words.length; i++) {
			var word = words[i];
			result += word.charAt(0).toUpperCase();
			result += word.substr(1);
			if (i < words.length -1);
				result += " "
		}

		return result;
	}

	function initials(str, maxLength) {
		var result = "";

		var words = str.replace("_", " ").split(" ");
		for (var i=0; i<words.length; i++) {
			result += words[i].trim().charAt(0).toUpperCase();
		}

		result = result.toUpperCase();
		if (isNaN(maxLength) == false) {
			if (result.length > maxLength) {
				result = result.substr(0, maxLength);
			}
		}

		return result;
	}

	function trimSlashes(str) {
		while (true) {
			var char = str.charAt(str.length-1);
			if (char == "/" || char == "\\") {
				str = str.substr(0, str.length-2);
			}
			else break;
		}

		return str;
	}

	function getPosition(element) {
		var pos = { x: 0, y: 0 };

		var parent = element;
		while (parent != null) {
			if (element.tagName == "BODY") {
				var scrollX = parent.scrollLeft || document.documentElement.scrollLeft;
				var scrollY = parent.scrollTop || document.documentElement.scrollTop;
				pos.x += (parent.offsetLeft - scrollX + parent.clientLeft);
				pos.y += (parent.offsetTop - scrollY + parent.clientTop);
			}
			else {
				pos.x += (parent.offsetLeft - parent.scrollLeft + parent.clientLeft);
				pos.y += (parent.offsetTop - parent.scrollTop + parent.clientTop);
			}
			parent = parent.offsetParent;
		}

		return pos;
	}

	function isArray(obj) {
		return obj instanceof Array;
	}

	function mixinDeep(source, dest) {
		return mixin(source, dest, clone);
	}

	function mixin(source, dest, copyFunc) {
		var name, s, i, empty = {};
		for(name in source){
			s = source[name];
			if(!(name in dest) || (dest[name] !== s && (!(name in empty) || empty[name] !== s))){
				dest[name] = copyFunc ? copyFunc(s) : s;
			}
		}
		return dest;
	}

	function clone(src) {
		if(!src || typeof src != "object" || Object.prototype.toString.call(src) === "[object Function]"){
			// null, undefined, any non-object, or function
			return src;	// anything
		}
		if(src.nodeType && "cloneNode" in src){
			// DOM Node
			return src.cloneNode(true); // Node
		}
		if(src instanceof Date){
			// Date
			return new Date(src.getTime());	// Date
		}
		if(src instanceof RegExp){
			// RegExp
			return new RegExp(src);   // RegExp
		}
		var r, i, l;
		if(src instanceof Array){
			// array
			r = [];
			for(i = 0, l = src.length; i < l; ++i){
				if(i in src){
					r.push(clone(src[i]));
				}
			}
		}
		else if (src._isAMomentObject) {
			r = new Date(src.valueOf());
		}
		else {
			// generic objects
			r = src.constructor ? new src.constructor() : {};
		}
		return mixin(src, r, clone);
	}

	function filterDeep(filter, object, key, levels) {
		levels = levels || 0;

		if (levels >= maxRecursion) {
			error("mixinDeep exceeded maxinum recursion levels (" + maxRecursion + ")");
			return null;
		}

		for (var k in object) {
			filter(object, k);
			var value = object[k];
			if (typeof value === "object") {
				filterDeep(filter, value, k, levels + 1);
			}
		}
	}

	function childOfParent(child, parent, maxLevels) {
		maxLevels = maxLevels | 100;
		while (child != null) {
			if (child == parent)
				return true;

			child = child.parentNode;

			if (--maxLevels < 0) break;
		}
		return false;
	}

	function removeSelection() {
		var selection = window.getSelection();
		if (selection)
			selection.removeAllRanges();
	}

	function goto(url) {
		if (url.charAt(0) != '/')
			url = '/' + url;

		window.location.hash = "#" + url;
	}

	function warn(msg) {
		msg = msg || null;

		var e = new Error('error');
	    var stack = e.stack.replace(/^[^\(]+?[\n$]/gm, '')
	        .replace(/^\s+at\s+/gm, '')
	        .replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@')
			.split("\n");

		//msg += "\n" + stack.join("\n");

	    console.error(msg);
	}

	function loadJson(url, data, cb) {
		if (data != null && typeof data === "function") {
			console.error("data should not be a function.");
		}
		if (cb != null && typeof cb !== "function") {
			console.error("cb is not a function.");
		}

		var callback = function(data) {
			if (typeof cb === "function") {
				cb(data);
			}
		};

		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			var str = this.responseText;

			var braceI = str.indexOf("{");
			str = "{" + str.substr(braceI + 1);

			var data = jsonToObj(str);

			if (braceI > 0) {
				console.warn(str.substr(0, braceI));
			}

			if (this.status != 200) {
				console.warn(this.status, data);
			}

			if (cb) callback(data);
		};

		xhr.onerror = function() {
			console.warn("loadJSON didn't complete", this);
			if (cb) cb({
				error: "An error occurred while loading.",
				response: this.responseText,
				statusText: this.statusText,
				status: -1
			});
		};

		var sending = data != null;
		var method = sending ? "POST" : "GET";
		xhr.open(method, url, true);

		var badSend = function() {
			console.warn("method failed " + url + " " + this.status)
		};

		if (sending) {
			xhr.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
			var postData = objToJson(data);

			console.log("Sending to " + url + "..." + (data ? "\n\t" + postData : ""));
			try {
				xhr.send(postData);
			}
			catch (e) { badSend(); }
		}
		else {
			console.log("Reading from " + url + "...");
			try {
				xhr.send();
			}
			catch (e) { badSend(); }
		}
	}

	function jsonToObj(str) {
		try {
			var obj = JSON.parse(str);
			return obj;
		}
		catch(e) {
			console.warn("NON-JSON", e, str);
			return null;
		}
	}

	function objToJson(obj) {
		obj = clone(obj);

		try {
			var str = JSON.stringify(obj);
			return str;
		}
		catch(e) {
			console.warn("ERROR", e);
			return null;
		}
	}

	function loadString(url, cb) {
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			var str = this.responseText;
			//console.log("loaded " + str);
			if (cb) cb(str);
		};

		xhr.onerror = function() {
			var msg = "Error: " + url + " didn't load."
			console.warn(msg, this.responseText);
			if (cb) cb(msg);
		};

		xhr.open("GET", url);
		console.log("loading " + url);
		xhr.send();
	}

	function loadHtml(url, container, data, cb) {
		loadString(url, function(html) {
			if (data) {
				html = inject(html, data);
			}
			container.innerHTML = html;

			var showElems = container.querySelectorAll("[show]");
			for (var i=0; i<showElems.length; i++) {
				var showElem = showElems[i];
				var showAtt = showElem.getAttribute("show");
				var showValue = lookup(data, showAtt);
				if (showValue == null) {
					showElem.style.display = "none";
				}
			}

			setTimeout(function(){
				if (cb) cb(html);
			}, 10);
		});
	}

	function inject(string, data) {
		var parts = string.split("{");

		if (parts.length > 1) {
			for (var i=1; i<parts.length; i++) {
				var part = parts[i];
				var endI = part.indexOf('}');
				if (endI > 0) {
					var lookupKey = part.substr(0, endI);
					var value = lookup(data, lookupKey);
					value = value || "";
					part = value + "" + part.substr(endI + 1);
					if (!value) {
						console.log("No value for " + lookupKey)
					}
					parts[i] = part;
				}
			}
		}

		return parts.join("");
	}

	function lookup(obj, path) {
		var paths = path.trim().split(".");

		var ref = obj;
		while (paths.length > 0) {
			var nextPath = paths.shift();
			ref = ref[nextPath];
			if (ref == null)
				break;
		}

		return ref;
	}

	function formSubmitter(form, action) {
		return function(e) {
			e.preventDefault();
			var data = formData(form);
			var actionName = form.getAttribute("submit");
			var actionFunc = lookup(window, actionName);
			actionFunc.apply(form, [data]);
			return false;
		}
	}

	function latchForms() {
		var forms = document.getElementsByTagName("form");
		for (var i=0; i<forms.length; i++) {
			var form = forms[i];

			var onsubmitMethod = formSubmitter(form);
			form.onsubmit = onsubmitMethod;

			var buttons = form.querySelectorAll('button[type="submit"]');
			for (var j=0; j<buttons.length; j++) {
				var button = buttons[j];
				button.addEventListener("touchstart", onsubmitMethod);
			}
		}
	}

	function formData(form) {
		var data = {};
		var fields = form.querySelectorAll("[name]");
		for (var i=0; i<fields.length; i++) {
			var field = fields[i];
			var key = field.getAttribute("name");
			var value = field.value;
			data[key] = value;
		}
		return data;
	}

	function fillClasses(className, html) {
		setTimeout(function() {
			var elems = document.getElementsByClassName(className);
			for (var i=0; i<elems.length; i++) {
				elems[i].innerHTML = html;
			}
		}, 50);
	}

	function contentPath(src) {
		var contentMatch = "content/";
		var contentI = src.indexOf(contentMatch);
		if (contentI >= 0) {
			src = src.substr(contentI + contentMatch.length);
		}
		src = "../content/" + src;

		/*/var currentPath = window.location.href;
		if (currentPath.charAt(currentPath.length) == "/") {
			currentPath = currentPath.substr(0, currentPath.length - 2);
		}

		var lastSlashI = currentPath.lastIndexOf("/");
		currentPath = currentPath.substr(0, lastSlashI);
		src = currentPath + "/" + src;
		*/
		return src;
	}

})();
