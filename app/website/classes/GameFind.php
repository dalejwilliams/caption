<?php
// Turnado by Dwill 2016

include_once('Module.php');
include_once('App.php');
include_once('Database.php');
include_once('ErrorCode.php');

class GameFind extends Module
{
	public $game = null;

	function run()
	{
		$this->game = App::requireVar('room');
		$this->loadAndPrint();
	}

	function load()
	{
		$game = $this->game;

		if ($game == null)
		{
			$this->error = "Game could not be found.";
			$this->errorCode = ErrorCode::Game_Not_Found;
			return;
		}

		App::log("Found game $game.");
		$result = self::findGame($game);

		$this->response = $result;
	}

	static function findGame($game)
	{
		if ($game == null)
		{
			return null;
		}

		$data = array(
			'game' => $game
		);

		$sql = "SELECT * FROM games WHERE game=:game";
		$result = Database::fetchOne($sql, $data);

		return $result;
	}
}

?>
