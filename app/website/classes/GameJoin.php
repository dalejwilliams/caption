<?php
// Turnado by Dwill 2016

include_once('Module.php');
include_once('App.php');
include_once('Database.php');
include_once('ErrorCode.php');
include_once('GameFind.php');

class GameJoin extends Module
{
	function run()
	{
		$game = strtoupper(App::requireVar('room'));
		$name = strtoupper(App::getVar('name'));

		$result = GameFind::findGame($game);

		$address = $result['address'];
		$url = 'http://' . $address . ':3011/player/#room=' . $game . '&name=' . $name;

		header('Location: ' . $url);
	}
}

?>
