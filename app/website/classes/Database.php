<?php
// DwillTV by Dwill 2016

include_once('App.php');
include_once('Utils.php');

class Database
{
	static public $dbh = null;
	static public $result = null;
	static public $error = null;

	static function open()
	{
		if (self::$dbh != null)
		{
			return;
		}

		try
		{
			if (App::$isLiveDB)
			{
				self::$dbh = new PDO("mysql:host=mysql.dalejwilliams.com;dbname=dwilltv", "dwilltvuser", "userofdwilltv");
			}
			else
			{
				App::log("DB-LOCAL");
				self::$dbh = new PDO("mysql:host=localhost;dbname=dwilltv", 'root', '');
			}
		}
		catch (Exception $e)
		{
			return die("DB OPEN: " . $e->getMessage());
		}
	}

	static function sqlInsertString($data)
	{
		$tokens = Utils::buildTokenArray($data);
		$keyStr = implode(',', $tokens);
		$valStr = ':' . implode(',:', $tokens);
		$str = "($keyStr) VALUES ($valStr)";
		return $str;
	}

	static function sqlUpdateString($data, $ignore)
	{
		$tokens = Utils::buildTokenArray($data, $ignore);
		$pairs = array();
		for ($i=0; $i<sizeof($tokens); $i++)
		{
			$token = $tokens[$i];
			array_push($pairs, "$token=:$token");
		}
		$str = implode(',', $pairs);
		return $str;
	}

	static function execute($sql, $data = null)
	{
		self::$error = null;
		self::$result = null;

		$logSQL = $sql;

		self::open();
		$sth = self::$dbh->prepare($sql);

		if (isset($data))
		{
			foreach ($data as $key => $value)
			{
				$bindKey = ':' . $key;
				$logValue = $value;

				$sth->bindValue($bindKey, $value);
				$logSQL = str_replace($bindKey, $logValue, $logSQL);
			}
		}

		App::log("Query: $logSQL");

		$execution = false;
		$error = null;
		try
		{
			$execution = $sth->execute();
		}
		catch (Exception $e)
		{
			$error = $e->getMessage();
		}

		if ($error != null)
		{
			return self::fail("DB EXEC ERROR: " . $errStr);
		}

		if ($execution == false)
		{
			return self::fail("DB EXEC ERROR.");
		}

		$rows = $sth->rowCount();
		if ($rows == 0)
		{
			return self::fail("DB EXEC: no result.");
		}

		return $sth;
	}

	static function insert($table, $data)
	{
		$insertStr = self::sqlInsertString($data);
		$sql = "INSERT INTO $table $insertStr";
		$sth = self::execute($sql, $data);
		return $sth != null;
	}

	static function upsert($table, $data, $updateKeys)
	{
		$insertStr = self::sqlInsertString($data);
		$updateStr = self::sqlUpdateString($data, $updateKeys);
		$sql = "INSERT INTO $table $insertStr ON DUPLICATE KEY UPDATE $updateStr";
		$sth = self::execute($sql, $data);
		return $sth != null;
	}

	static function fetchOne($sql, $data = null, $method = null)
	{
		$sth = self::execute($sql, $data);
		if ($sth == null)
		{
			return self::fail("No result.");
		}

		if ($method == null)
		{
			$method = PDO::FETCH_ASSOC;
		}

		$result = $sth->fetch($method);
		return self::succeed($result);
	}

	static function fetchAll($sql, $data = null, $method = null)
	{
		App::beginOpInfo();
		$sth = self::execute($sql, $data);
		if ($sth == null)
		{
			return self::fail("No results.");
		}

		if ($method == null)
		{
			$method = PDO::FETCH_ASSOC;
		}

		$result = $sth->fetchAll($method);
		App::logOpInfo("Fetch all");

		return self::succeed($result);
	}

	static function fetchCount($sql, $data = null)
	{
		$result = self::fetchOne($sql, $data, PDO::FETCH_NUM);
		return $result[0];
	}

	static function fail($error)
	{
		App::log($error);
		self::$error = $error;
		self::$result = null;
		return null;
	}

	static function succeed($data)
	{
		self::$error = null;
		self::$result = $data;
		return $data;
	}
}

?>
