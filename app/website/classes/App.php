<?php
// DwillTV by Dwill 2016

App::Init();

class App
{
	static public $output = null;
	static public $isLiveServer = false;
	static public $isLiveDB = false;
	static public $startTime = 0;
	static public $opInfo = null;

	static function Init()
	{
		self::$startTime = microtime(true);
		self::$isLiveDB = self::getVar('local') != 'true';

		$serverPre = substr($_SERVER['SERVER_ADDR'], 0, 3);
		self::$isLiveServer = $serverPre != '192' && $serverPre != '::1' && $serverPre != '127';

		header("Content-Type: application/json");
		ini_set('html_errors', false);

		if (self::$isLiveServer == false)
		{
			ini_set('display_startup_errors',1);
			ini_set('display_errors',1);
			error_reporting(-1);
		}

		self::$output = array(
			'result' => null
		);
	}

	static function getVar($key, $default=null)
	{
		$result = $default;
		$mode = null;

		if (isset($_POST[$key]))
		{
			$result = $_POST[$key];
			$mode = 'POST';
		}

		if (isset($_GET[$key]))
		{
			$result = $_GET[$key];
			$mode = 'GET';
		}

		if (self::$isLiveServer == false)
		{
			$logValue = $result == null ? 'NULL' : $result;
			if ($mode == null)
			{
				self::log("Var: $key defaulted to $logValue");
			}
			else
			{
				self::log("Var: $key is $logValue");
			}

		}

		return $result;
	}

	static function getVars($keys, $default=null)
	{
		$result = array();

		for ($i=0; $i < count($keys); $i++)
		{
			$key = $keys[$i];
			$value = self::getVar($key, $default);

			if ($value == "" || $value == null) continue;

			$result[$key] = $value;
		}

		return $result;
	}

	static function requireVar($key)
	{
		$value = self::getVar($key, null);
		if ($value == null)
		{
			self::fail("Required $key absent.");
		}
		return $value;
	}

	static function requireVars($keys)
	{
		$values = self::getVars($keys, null);
		foreach ($values as $key => $value)
		{
			if ($value == null)
			{
				self::fail("Required $key in set absent.");
			}
		}
		return $value;
	}

	static function log($msg)
	{
		if (self::$isLiveServer)
		{
			return;
		}

		if (isset(self::$output['log']) == false)
		{
			self::$output['log'] = array();
		}

		array_push(self::$output['log'], $msg);
	}

	static function succeed($data = null)
	{
		if (isset($data))
		{
			self::$output['result'] = $data;
		}

		if (isset(self::$output['result']) == false)
		{
			self::$output['result'] = null;
		}

		self::printOutput();
	}

	static function fail($error, $errorCode = 1)
	{
		if (isset(self::$output['result']))
		{
			unset(self::$output['result']);
		}

		self::log($error);
		self::$output['error'] = $error;
		self::$output['errorCode'] = $errorCode;

		self::printOutput();
	}

	static function printOutput()
	{
		$generateTime = microtime(true) - self::$startTime;

		self::$output['generateTime'] = $generateTime;
		self::$output['uts'] = time();

		if (self::$output['result'] == null)
		{
			unset(self::$output['result']);
		}

		if (self::$isLiveServer)
		{
			echo json_encode(self::$output);
		}
		else
		{
			echo json_encode(self::$output, JSON_PRETTY_PRINT);
		}

		die();
	}

	static function beginOpInfo()
	{
		self::$opInfo = array();
		self::$opInfo['startTime'] = microtime(true);
		self::$opInfo['startMemory'] = memory_get_usage();
	}

	static function getOpInfo()
	{
		self::$opInfo['time'] = microtime(true) - self::$opInfo['startTime'];
		self::$opInfo['memory'] = memory_get_usage() - self::$opInfo['startMemory'];
		return self::$opInfo;
	}

	static function logOpInfo($prefix)
	{
		$info = self::getOpInfo();
		$time = round($info['time'] * 10) / 10;
		$memory = round($info['memory'] / 1024 / 1024 * 10) / 10;
		self::log("OpInfo: > $prefix: time: $time s, memory: $memory mb");
	}

	static function isCacheValid($key, $maxAge)
	{
		$path = self::getCachePath($key);
		if (file_exists($path) == false)
		{
			self::log("Invalid cache path $path.");
			return false;
		}

		$now = time();
		$saveTime = filemtime($path);

		return $now - $saveTime < $maxAge;
	}

	static function getFromCache($key, $maxAge)
	{
		$path = self::getCachePath($key);

		if (self::isCacheValid($key, $maxAge) == false)
		{
			unlink($path);
			return null;
		}

		$cacheStr = file_get_contents($path);
		$data = json_decode($cacheStr);

		$data['modified'] = filemtime($path);

		return $data;
	}

	static function storeInCache($key, $data)
	{
		$path = self::getCachePath($key);

		$dirPath = self::getCachePath('');
		if (file_exists($dirPath) == false)
		{
			mkdir($dirPath);
		}

		$json = json_encode($data);

		$str = $json;
		file_put_contents($path, $str);

		self::log("Stored in cache " . $key);
	}

	static function getCachePath($key)
	{
		return './cache/' . $key;
	}
}

?>
