<?php
// DwillTV by Dwill 2016

class Utils
{
	static function buildTokenArray($data, $ignore = null)
	{
		$tokens = array();

		$keys = array_keys($data);
		$keyCount = sizeof($keys);

		for ($i=0; $i<$keyCount; $i++)
		{
			$key = $keys[$i];
			if ($ignore != null && self::isMatchOrInSet($key, $ignore))
			{
				continue;
			}
			array_push($tokens, $key);
		}

		return $tokens;
	}

	static function objectProperty($object, $key, $default=null)
	{
		if (isset($object[$key]))
			return $object[$key];

		return $default;
	}

	static function isMatchOrInSet($value, $matchOrSet)
	{
		if (is_array($matchOrSet) == false)
		{
			return $value == $matchOrSet;
		}

		$count = sizeof($matchOrSet);
		for ($i=0; $i<$count; $i++)
		{
			if ($matchOrSet[$i] == $value)
			{
				return true;
			}
		}

		return false;
	}

}

?>
