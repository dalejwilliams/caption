<?php

abstract class ErrorCode
{
    const None = 0;
    const Unknown = 1;
    const Game_Not_Found = 10;
    const Game_Started = 11;
    const Game_Full = 12;
    const DB_None = 20;
    const DB_Access = 21;
    const DB_Error = 22;
}

?>
