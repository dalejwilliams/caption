<?php
// DwillTV by Dwill 2016

include_once('Module.php');
include_once('App.php');
include_once('Database.php');

class GameInfo extends Module
{
	function run()
	{
		$this->loadAndPrint();
	}

	function load()
	{
		$result = self::readInfo();

		if ($result == null)
		{
			$this->error = "No games found.";
			return;
		}

		$this->response = $result;
	}

	static function readInfo()
	{
		$sql = "SELECT * FROM games ORDER BY uts LIMIT 1000";
		$result = Database::fetchAll($sql);

		if (sizeof($result) < 1)
		{
			return null;
		}

		return $result;
	}
}

?>
