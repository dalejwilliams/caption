<?php
// DwillTV by Dwill 2016

include_once('Module.php');
include_once('App.php');
include_once('Database.php');
include_once('GameFind.php');
include_once('ErrorCode.php');

class GameCreate extends Module
{
	function run()
	{
		$this->loadAndPrint();
	}

	function load()
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		if ($ip == '::1') {
			$ip = '127.0.0.1';
		}

		$game = '';
		$chars = 4;
		while ($chars-- > 0) {
			$charI = rand(0, 25) + 65;
			$char = chr($charI);
			$game .= $char;
		}

		$data = array(
			'game' => $game,
			'address' => $ip
		);

		$result = Database::upsert('games', $data, 'address');

		$this->response = GameFind::findGame($game);
  	}
}

?>
