<?php
// DwillTV by Dwill 2016

include_once("App.php");

class Module
{
	public $response = null;
	public $error = null;
	public $errorCode = 1;

	function run()
	{
		$this->loadAndPrint(null);
	}

	function load()
	{
		$error = "Module not implemented.";
	}

	function loadAndPrint()
	{
		$this->load();
		$this->printOutput();
	}

	function printOutput()
	{
		if ($this->error != null)
		{
			App::fail($this->error, $this->errorCode);
			return;
		}
		App::succeed($this->response);
	}
}

?>
