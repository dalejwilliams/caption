// Caption: (c) 2016-2017 copyright 2015 Dale J Williams
(function() {
	'use strict';

	var events = window.events = {
		EventList: EventList,
		onUpdate: new EventList(),
		onPress: new EventList(),
		onRelease: new EventList(),
		onDrag: new EventList(),
		onDrop: new EventList(),
		onResize: new EventList(),
		onKey: new EventList(),
		start: start,
		stop: stop,
		press: {
			isDown: false,
			x: 0,
			y: 0,
			holdX: 0,
			holdY: 0,
			deltaX: 0,
			deltaY: 0,
			downX: 0,
			downY: 0,
			upX: 0,
			upY: 0,
			prevX: 0,
			prevY: 0
		},
		removeFrom: removeFrom
	};

	var playing = false;
	var stepTime = 33;
	start();

	function start() {
		playing = true;
		nextStep();
	}

	function stop() {
		playing = false;
	}

	function update() {
		if (playing == false) return;

		var press = events.press;
		press.deltaX = isNaN(press.x) ? 0 : press.x - press.prevX;
		press.deltaY = isNaN(press.y) ? 0 : press.y - press.prevY;
		press.prevX = press.x;
		press.prevY = press.y;

		if (press.isDown) {
			if (press.target != null) {
				press.target.focus();
			}
			//window.getSelection().removeAllRanges();
		}

		events.onUpdate.dispatch(null, press);
		nextStep();
	}

	function nextStep() {
		clearTimeout(self.timer);
		if (window.requestAnimationFrame) {
			window.requestAnimationFrame(update);
		}
		else {
			self.timer = setTimeout(function() {
				clearTimeout(self.timer);
				update();
			}, stepTime);
		}
	}

	function mouseDown(e) {
		var posX = e.clientX;
		var posY = e.clientY;
		updatePress(posX, posY, true, e.target);
	}

	function mouseUp(e) {
		var posX = e.clientX;
		var posY = e.clientY;
		updatePress(posX, posY, false);
	}

	function mouseMove(e) {
		var posX = e.clientX;
		var posY = e.clientY;
		updatePress(posX, posY);
	}

	function touchStart(e) {
		if (e.touches.length < 1) return;
		var touch = e.touches[0];
		updatePress(e.pageX, e.pageY, true, e.target);
	}

	function touchEnd(e) {
		if (e.touches.length < 1) return;
		var touch = e.touches[0];
		updatePress(e.pageX, e.pageY, false);
	}

	function touchMove(e) {
		if (e.touches.length < 1) return;
		var touch = e.touches[0];
		updatePress(e.pageX, e.pageY);
	}

	function updatePress(x, y, isDown, target) {
		var press = events.press;

		var isPressChange = typeof isDown !== "undefined";

		if (isPressChange) {
			press.isDown = isDown;
			if (isDown) {
				press.downX = x;
				press.downY = y;
				press.target = target;
			}
			else {
				press.upX = x;
				press.upY = y;
			}
			press.isDown = isDown;
		}

		press.x = x;
		press.y = y;
		press.holdX = press.isDown ? x - press.downX : 0;
		press.holdY = press.isDown ? y - press.downY : 0;

		if (isPressChange) {
			if (isDown) {
				events.onPress.dispatch(target, press);
			}
			else {
				events.onRelease.dispatch(target, press);
			}
		}
	}

	function removeFrom(element/*s...*/) {
		events.onUpdate.removeFrom(element);
		events.onPress.removeFrom(element);
		events.onRelease.removeFrom(element);
		events.onDrag.removeFrom(element);
		events.onDrop.removeFrom(element);

		if (arguments.length > 1) {
			for (var i=1; i<arguments.length; i++) {
				removeFrom(arguments[i]);
			}
		}
	}

	function resize(e) {
		events.onResize.dispatch(null, e);
	}

	function keyDown(e) {
		events.onKey.dispatch(null, e.keyCode);
	}

	window.addEventListener('mousedown', mouseDown);
	window.addEventListener('mouseup', mouseUp);
	window.addEventListener('mousemove', mouseMove);
	window.addEventListener('touchstart', touchStart);
	window.addEventListener('touchend', touchEnd);
	window.addEventListener('touchmove', touchMove);
	window.addEventListener('resize', resize);
	window.addEventListener('keydown', keyDown);

	/* --------------------------
	 			TYPES
	----------------------------*/
	function EventList() {
		this.items = [];
	}
	var p = EventList.prototype;
	p.add = function(method, element/*s...*/) {
		if (arguments.length > 1 && element == null) {
			throw new Error("Couldn't add null element.");
		}
		if (this.has(method, element)) return;
		this.items.push({
			method: method,
			element: element
		});
		if (arguments.length > 2) {
			for (var i=2; i<arguments.length; i++) {
				this.add(method, arguments[i]);
			}
		}
	};
	p.index = function(method, element) {
		var i = this.items.length;
		while (i-- > 0) {
			if (this.items[i].method == method && this.items[i].element == element)
				return i;
		}
		return -1;
	};
	p.has = function(method, element) {
		return this.index(method, element) >= 0;
	};
	p.remove = function(method, element/*s...*/) {
		var index = this.index(method, element);
		if (index < 0) return;
		this.items.splice(index, 1);
		if (arguments.length > 2) {
			for (var i=2; i<arguments.length; i++) {
				this.remove(method, arguments[i]);
			}
		}
	};
	p.removeFrom = function(element/*s...*/) {
		var i = this.items.length;
		while (i-- > 0) {
			if (utils.childOfParent(this.items[i].element, element))
				this.items.splice(i, 1);
		}
		if (arguments.length > 1) {
			for (var i=1; i<arguments.length; i++) {
				this.removeFrom(arguments[i]);
			}
		}
	};
	p.removeAll = function(method) {
		var i = this.items.length;
		while (i-- > 0) {
			if (this.items[i].method == method)
				this.items.splice(i, 1);
		}
	};
	p.dispatch = function(target, data) {
		for (var i=0; i<this.items.length; i++) {
			var item = this.items[i];
			if (target == null || utils.childOfParent(target, item.element)) {
				item.method.apply(target, [data]);
			}
		}
	};
})();
