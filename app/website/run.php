<?php
// Turnado by Dwill 2016

include_once('classes/App.php');

$action = App::getVar('action');
doAction($action);

function doAction($action)
{
	App::log("Performing action $action...");
	switch ($action)
	{
		case 'create':
			include_once('classes/GameCreate.php');
			runModule(new GameCreate());
			return;

		case 'join':
			include_once('classes/GameJoin.php');
			runModule(new GameJoin());

		case 'find':
			include_once('classes/GameFind.php');
			runModule(new GameFind());

		case 'info':
			include_once('classes/GameInfo.php');
			runModule(new GameInfo());

		default:
			die("Hi!");
	}
}

function runModule($module)
{
	$module->run();
}

?>