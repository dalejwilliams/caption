'use strict';

module.exports = class CaptionPlayer {

    constructor(client, room, name) {
        this.client = client;
        this.name = room;
        this.room = name;
        this.resetPoints();
        this.handlers = {
            "start-game":   this.onStartGame,
            "answer":       this.onAnswer,
            "vote":         this.onVote
        }
    }

    resetPoints() {
        this.points = 0;
        this.votes = 0;
        this.conceeds = 0;
        this.cocksures = 0;
    }

    connect() {
        this.client.handler = this;
        this.client.send("auth", this.room.getStats());
    }

    roundReveal() {
        this.answers = [];
        this.roundDone = false;
        this.client.send("round-reveal", this.roundData);
    }

    onMessage(type, data) {
        console.log("player message", type, data);

        var handler = this.handlers[type];
        if (handler != null) {
            handler.apply(this, [data]);
        }
        else {
            console.error("No handler for player " + this.name);
        }
    }

    onStartGame(msg) {
        this.room.startGame();
    }

    onAnswer(msg) {
        this.room.playerAnswered(this, msg);
    }

    onVote(msg) {
        this.room.playerVoted(this, msg);
    }

    addPoints(points, votes, conceeded, cocksure) {
        this.points += points || 0;
        this.votes += votes || 0;
        if (conceeded === true) {
            this.conceeds++;
        }
        if (cocksure == true) {
            this.cocksures++;
        }
    }
}
