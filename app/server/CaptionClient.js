'use strict';

module.exports = class CaptionSock {

    constructor(server, index, connection) {
        this.server = server;
        this.index = index;
        this.handler = null;

        if (connection) {
            this.connect(connection);
        }
    }

    connect(connection) {
        this.connection = connection;
        connection.on('message', this.onMessage.bind(this));
        connection.on('close', this.onClose.bind(this));
    }

    onMessage(message) {
        console.log(this.getName() + " got message.");

        var obj = null;
        try {
            var obj = JSON.parse(message.utf8Data);
        }
        catch (e) {
            console.warn("non json ", message);
            return;
        }

        var type = obj.type;

        if (this.handler != null) {
            this.handler.onMessage(type, obj.data);
        }
        else {
            if (type == "room-auth") {
                this.server.addRoom(this);
                return;
            }

            if (type == "player-auth") {
                this.server.addPlayer(this, obj.data);
                return;
            }

            this.send("error", {
                reason: "Couldn't connect."
            });
        }
    }

    onClose(connection) {
        console.log(this.getName() + " closed.");
    }

    send(type, data) {
        var obj = {
            type: type,
            data: data
        }
        var str = JSON.stringify(obj);
        this.connection.send(str);
    }

    close() {
        this.connection.close();
    }

    getName() {
        return "Client " + this.index;
    }

}
