'use strict';

const RSSRound = require("./RSSRound");

const NewsUrls = [
    "http://feeds.foxnews.com/foxnews/scitech?format=xml",
    "http://feeds.foxnews.com/foxnews/health?format=xml",
    "http://www.huffingtonpost.com/feeds/verticals/celebrity/index.xml",
    "http://www.huffingtonpost.com/feeds/index.xml"  
];

module.exports = class NewsishRound extends RSSRound {

    load(cb) {
        this.loadAll(NewsUrls, function(data) {
            cb(data);
        });
    }

    getName() {
        return "News-ish";
    }

};