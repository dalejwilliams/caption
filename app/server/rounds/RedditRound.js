'use strict';

const RSSRound = require("./RSSRound");

const ReditUrls = [
    "http://www.reddit.com/r/random.rss"
];

module.exports = class RedditRound extends RSSRound {

    load(cb) {
        this.loadAll(ReditUrls, cb);
    }

    getName() {
        return "RedditSays";
    }

};