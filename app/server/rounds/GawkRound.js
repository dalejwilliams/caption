'use strict';

const RSSRound = require("./RSSRound");

const GawkUrls = [
    "http://feeds.gawker.com/lifehacker/full",
    "http://feeds.gawker.com/io9/full"
];

module.exports = class GawkRound extends RSSRound {

    load(cb) {
        this.loadAll(GawkUrls, cb);
    }

    getName() {
        return "Gawking";
    }

};