'use strict';

const Round = require("./Round");
const utils = require("../utils");

const NewsUrls = [
    "http://feeds.foxnews.com/foxnews/scitech?format=xml",
    //"http://feeds.foxnews.com/foxnews/health?format=xml",
   // "http://www.huffingtonpost.com/feeds/verticals/celebrity/index.xml",
    "http://www.huffingtonpost.com/feeds/index.xml",
    "http://rss.cnn.com/rss/edition_technology.rss"
];

module.exports = class NewsBlankRound extends Round {

    load(cb) {
        this.loadAll(NewsUrls, cb);
    }

    loadItem(url, cb) {
        this.loadRssTitles(url, function(titles) {

            for (var title of titles) {
                title = title.replace(": Report", "");

                var words = title.split(" ");

                var startWord = Math.ceil(words.length * 0.3);

                if (startWord < 1)
                    continue;
                
                words = words.slice(startWord);

                title = "_____ " + words.join(" ");
                
                if (title.length < 5)
                    continue;

                this.data.push({
                    text: title
                })
            }

            cb();

        }.bind(this));
    }

    getName() {
        return "Incomplete Headline";
    }

};