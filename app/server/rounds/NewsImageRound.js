'use strict';

const Round = require("./Round");
const utils = require("../utils");
const request = require("request");

const NewsImages = [
    "http://feeds.foxnews.com/foxnews/scitech?format=xml",
    "http://feeds.foxnews.com/foxnews/politics?format=xml",
    "http://feeds.foxnews.com/foxnews/politics?format=xml",
    "http://rss.cnn.com/rss/edition_technology.rss",
    "http://feeds.foxnews.com/foxnews/internal/travel/mixed"
];

module.exports = class NewsImageRound extends Round {

    load(cb) {
        this.loadAll(NewsImages, cb);
    }

    loadItem(url, cb) {
        request(url, function(err, res, str) {
            if (err) {
                console.error(err);
                return cb([]);
            }

            var images = utils.grok(str, '<media:content url="', '"');
            for (var image of images) {
                this.data.push({
                    image: image
                });
            }

            cb();

        }.bind(this));
    }

    getName() {
        return "Caption This";
    }

};