'use strict';

const Round = require("./Round");
const utils = require("../utils");
const request = require("request");

module.exports = class BuzzFoodRound extends Round {

    load(cb) {
        this.loadAll(["http://www.buzzfeed.com"])
    }

    loadAll(urls, cb) {
        this.pairCount = this.room.pairs.length;
        this.data = [];
        this.urls = urls;
        this.loadNext(cb);
    }

    loadNext(cb) {
        this.loadSource(function() {
            if (this.data.length < this.pairCount) {
                //this.loadNext(cb);
            }
            else {
                cb(this.data);
            }
        }.bind(this));
    }

    loadSource(cb) {
        var urlI = Math.floor(Math.random() * this.urls.length);
        var url = this.urls[urlI];

        var results = [];

        request(url, function(err, res, str) {
            if (err) {
                console.error(err);
                return cb([]);
            }

            str = unescape(str);
            str = str.substr(str.indexOf('class="grid-posts'));
            str = str.substr(0, str.indexOf("</ul"));

            //console.log("Got " + str);

            var titles = utils.grokElem(str, 'class="lede__link"', '</a>');
            for (var i=0; i<titles.length; i++) {
                this.data.push(titles[i]);
            }

            console.log(titles);

            cb();

        }.bind(this));
    }

};