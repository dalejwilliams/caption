'use strict';

const Round = require("./Round");
const request = require("request");

module.exports = class CatRound extends Round {

    load(cb) {
        var pairCount = this.room.pairs.length;
        var data = [];

        var apiKey = "MTMwMDc1";

        var url = "http://thecatapi.com/api/images/get?api_key=" + apiKey +
            "&format=xml&results_per_page=" + pairCount + 
            "&category!=space";

        console.log("loading " + url);

        request(url, function(err, res, str) {
            if (err) {
                console.error(err);
                return cb([]);
            }

            var parts = str.split("<url>");
            if (parts.length < 2) {
                console.log("No parts.");
                return cb([]);
            }

            for (var i=1; i<parts.length; i++) {
                var url = parts[i];
                url = url.substr(0, url.indexOf("</url>"));
                var obj = {
                    image: url
                }
                data.push(obj);
            }

            cb(data);
        });
    }

    getName() {
        return "Cat Captions";
    }

}
