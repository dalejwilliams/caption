'use strict';

const request = require("request");

var Round = module.exports = class Round {

    constructor(room) {
        this.room = room;
    }

    load(cb) {
         throw new Error("Can't load Round.");
    }

    loadAll(urls, cb) {
        this.pairCount = this.room.pairs.length;
        this.data = [];
        this.urls = urls;
        this.loadCount = 0;
        this.loadNext(cb);
    }

    loadNext(cb) {
        if (this.loadCount++ > 20) {
            throw new Error("20 loads.");
        }

        var urlI = Math.floor(Math.random() * this.urls.length);
        var url = this.urls[urlI];

        this.loadItem(url, function() {
            if (this.data.length < this.pairCount) {
                this.loadNext(cb);
            }
            else {

                while (this.data.length > this.pairCount) {
                    var removeI = Math.floor(Math.random() * this.data.length);
                    this.data.splice(removeI, 1);
                }

                cb(this.data);
            }
        }.bind(this));
    }

    loadItem(url, cb) {
        throw new Error("Can't load Round.");
    }

    getName() {
        return "No round.";
    }

    loadRssTitles(url, cb) {
        var titles = [];

        request(url, function(err, res, str) {
            if (err) {
                console.error(err);
                return cb([]);
            }

            str = str.substr(str.indexOf("<item>") + 1);

            var parts = str.split("<title>");
            if (parts.length < 2) {
                console.log("No parts.");
                return cb([]);
            }

            for (var i=1; i<parts.length; i++) {
                var title = parts[i];
                title = title.substr(0, title.indexOf("</title>"));
                title = title.replace("<![CDATA[", "");
                title = title.replace("]]>", "");

                titles.push(title);
            }

            cb(titles);

        }.bind(this));
    }

    debug() {
        this.room = { pairs:[1,0] };
        this.load(function(data) { console.log(data) });
    }

}

Round.modules = [];

Round.addModule = function(moduleName, weight) {
    var moduleClass = require("./" + moduleName);
    moduleClass.weight = isNaN(weight) ? 1 : weight;
    Round.modules.push(moduleClass);
    return moduleClass;
}

Round.select = function(room) {
    var totalWeight = 0;
    var moduleClass = null;

    for (var i=0; i<Round.modules.length; i++) {
        totalWeight += Round.modules[i].weight;
    }

    var selectedWeight = Math.random() * totalWeight;
    var weight = 0;
    for (var i=0; i<Round.modules.length; i++) {
        weight += Round.modules[i].weight;
        if (weight >= selectedWeight) {
            moduleClass = Round.modules[i];
            break;
        }
    }

    if (moduleClass == this.lastModuleClass) {
        return Round.select(room);
    }

    this.lastModuleClass = moduleClass;

    var module = new moduleClass(room);

    console.log("Selected " + module.getName());

    return module;
};

Round.addModule("CatRound", 0.6);
Round.addModule("NewsBlankRound");
Round.addModule("NewsImageRound");
Round.addModule("RandomWordRound", 0.8);
Round.addModule("DesignQuoteRound", 0.9);

Round.debug = function(name) {
    var module = null;
    if (name != null) {
        var moduleClass = Round.addModule(name);
        module = new moduleClass();
    }
    else {
        module = Round.select();
    }
    module.debug();
}
