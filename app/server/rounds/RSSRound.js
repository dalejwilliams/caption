'use strict';

const Round = require("./Round");

module.exports = class RSSRound extends Round {

    loadItem(cb) {
        var urlI = Math.floor(Math.random() * this.urls.length);
        var url = this.urls[urlI];

        var results = [];

        this.loadRssTitles(url, function(titles) {

            var repeats = 0;

            while (this.data.length < this.pairCount) {

                var halfLength = Math.floor(titles.length / 2);
                var titleI = Math.floor(Math.random() * halfLength) + halfLength;
                var title = titles[titleI];

                for (var i=0; i<this.data.length; i++) {
                    if (this.data[i].title == title) {
                        repeats++;
                        if (repeats > 4) {
                            cb();
                        }
                        continue;
                    }
                }

                this.data.push({
                    text: title
                });

            }

            cb();

        }.bind(this));
    }

};