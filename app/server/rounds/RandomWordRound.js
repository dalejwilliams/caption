'use strict';

const URL = "http://www.setgetgo.com/randomword/get.php";

const Round = require("./Round");
const request = require("request");

module.exports = class RandomWordRound extends Round {

    load(cb) {
        this.loadAll([URL], cb);
    }

    loadItem(url, cb) {
        request(url, function(err, res, str) {
            if (err) {
                console.error(err);
                return cb([]);
            }

            if (str.length < 3)
                return cb();

            this.data.push({
                text: str.toUpperCase()
            });

            cb();
        }.bind(this));
    }

    getName() {
        return "Use in a Sentence";
    }

};