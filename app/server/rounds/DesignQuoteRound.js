'use strict';

const URL = "http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=10";

const Round = require("./Round");
const request = require("request");

module.exports = class DesignQuoteRound extends Round {

    load(cb) {
        this.loadAll([URL], cb);
    }

    loadItem(url, cb) {
        request(url, function(err, res, str) {
            if (err) {
                console.error(err);
                return cb([]);
            }

            var json = JSON.parse(str);

            var entries = json;
            while (entries.length > 0) {
                var entryI = Math.floor(entries.length * Math.random());
                var entry = entries[entryI];
                entries.splice(entryI, 1);

                var str = entry.content;

                 if (str.length < 20)
                    continue;

                if (str.length > 100)
                    continue;

                var startI = str.indexOf("<p>");
                if (startI >= 0) {
                    str = str.substr(startI + 3);
                }
                var endI = str.indexOf("</p>");
                if (endI >= 0) {
                    str = str.substr(0, endI);
                }

                var periodI = str.indexOf(".");
                if (periodI >= 0) {
                    str = str.substr(0, periodI+1);
                }

                if (str.toLowerCase().indexOf("design") < 0)
                    continue;

                str = str.replace(/design/ig, '_____');

                this.data.push({
                    text: str
                });
            }

            cb();
        }.bind(this));
    }

    getName() {
        return "Design Quotes";
    }

};