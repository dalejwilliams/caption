/*
CAPTION: A game by Dale J Williams
*/
'use strict';

const WebSocketServer = require('websocket').server;
const http = require('http');
const CaptionClient = require('./CaptionClient');
const CaptionRoom = require('./CaptionRoom');
const CaptionPlayer = require('./CaptionPlayer');
const request = require("request");

module.exports = class CaptionServer {

	constructor(port) {
		this.port = port;

		this.httpServer = null;
		this.wsServer = null;
		this.clients = [];
		this.rooms = [];

		//this.hostRemote();
		this.hostLocal();
	}

	hostLocal(cb) {
		this.isLocal = true;

		require('dns').lookup(require('os').hostname(), function(err, add, fam) {

			this.name = CaptionRoom.generateCode();
			this.ip = add;
			this.url = "http://" + add;
			console.log("ip is " + this.ip);
			this.start();

		}.bind(this));
	}

	hostRemote(cb) {
		this.isLocal = false;

		request("http://tv.dwilderbeest.com/run.php?action=create", function(err, res, str) {
            if (err) {
                console.error(err);
                return cb([]);
            }

            var data = null;
            try { data = JSON.parse(str); }
            catch (e) {}

            console.log(str);

            if (data == null) {
                console.error("GOT: ", str);
                return cb([]);
            }

            var result = data.result;

            this.name = result.game;
			this.ip = add;
			this.url = "tv.dwilderbeest.com";
            console.log("Game registered " + this.name);

			this.start();
        }.bind(this));
	}

	start() {
		console.log("Starting CaptionServer: " + this.url)
		this.httpServer = http.createServer(function(request, result) {

		});
		this.wsServer = new WebSocketServer({
			httpServer: this.httpServer
		});

		this.wsServer.on('request', this.onRequest.bind(this));

		this.httpServer.listen(this.port, "0.0.0.0", function() {
			console.log(this._connectionKey)
		});
		console.log("CaptionServer started on port " + this.ip + ":" + this.port);
	}

	onRequest(request) {
		var connection = request.accept(null, request.origin);
		var index = this.clients.length;
		var client = new CaptionClient(this, index, connection);
		this.clients.push(client);
	}

	getRoom(roomName) {
		roomName = roomName.toUpperCase();
		var i = this.rooms.length;
		while (i--) {
			var room = this.rooms[i];
			if (room.name == roomName)
				return room;
		}
		return null;
	}

	addRoom(client) {
		var room = new CaptionRoom(client);
		if (this.isLocal) {
			this.localRoom = room;
		}
		this.rooms.push(room);
	}

	addPlayer(client, data) {
		var room = this.isLocal ? this.localRoom : this.getRoom(data.room);
		var name = data.name.toUpperCase();
		if (room != null) {
			for (var i=0; i<room.players.length; i++) {
				if (room.players[i].name == name) {
					client.send("error", {
						error: "Pick a new name!",
						info: "There's already some-one here called " + name
					});
					client.close();
					return;
				}
			}
			var player = new CaptionPlayer(client, name, room);
			room.addPlayer(player);
			return;
		}
		else {
			client.send("error", {
				error: "Can't join in!",
				info: "There's no room called " + data.room.toUpperCase()
			});
			client.close();
		}
	}

}
