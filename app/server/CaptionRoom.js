'use strict';

const request = require("request");
const Round = require("./rounds/Round");
const utils = require("./utils");

const Times = {
    GAME_START: 5000,
    ROUND_START: 3000,
    NEXT_QUESTION: 5000,
    VOTE_START: 1000,
    NEXT_VOTE: 5000,
    RESULTS: 5000
};

module.exports = class CaptionRoom {

    constructor(client, roomCode) {
        this.client = client;
        this.players = [];
        this.roundCount = 3;
        this.handlers = {
            "answer-time-up": this.onAnswerTimeUp,
            "vote-time-up":   this.onVoteTimeUp
        };
        this.auth(roomCode);
    }

    auth(roomCode) {
        this.name = roomCode || CaptionRoom.generateCode();
        /*
        
        console.log("New host named " + this.name);
        if (this.client) {
            this.client.handler = this;
            this.client.send("auth", {
                room: this.name,
                serverIP: this.client.server.ip
            });
        }
        else {
            console.error("No client for room.");
        }
        */

        if (this.client) {
            this.client.handler = this;
        }

        /*request("http://tv.dwilderbeest.com/run.php?action=create", function(err, res, str) {
            if (err) {
                console.error(err);
                return cb([]);
            }

            var data = null;
            try { data = JSON.parse(str); }
            catch (e) {}

            console.log(str);

            if (data == null) {
                console.error("GOT: ", str);
                return cb([]);
            }

            var result = data.result;

            this.name = result.game;
            this.address = result.address;
            console.log("Game registered " + this.name);

            this.client.send("auth", {
                room: this.name,
                serverIP: this.address
            });
        }.bind(this));*/

        this.client.send("auth", {
            room: this.name,
            ip: this.client.server.ip,
            url: this.client.server.url
        });
    }

    static generateCode() {
        var code = "";
        var chars = 4;
        while (chars-- > 0) {
            var charI = 65 + Math.floor(Math.random() * 26);
            code += String.fromCharCode(charI);
        }
        return code;
    }

    onMessage(type, data) {
        console.log("host message", type, data);

        var handler = this.handlers[type];
        if (handler != null) {
            handler.apply(this, [data]);
        }
        else {
            console.error("No handler for player " + this.name);
        }
    }

    addPlayer(player) {
        this.players.push(player);
        this.indexPlayers();
        player.connect();
        this.sendToAll("players", this.getStats());
    }

    indexPlayers() {
        for (var i=0; i<this.players.length; i++) {
            this.players[i].index = i;
        }
    }

    startGame() {
        this.sendToAll("game-start", this.getStats());
        this.failures = 0;
        this.round = 1;
        this.delay(Times.GAME_START, this.roundStart);
    }

    roundStart(roundN) {
        this.round = roundN || this.round;
        this.playersDone = [];
        this.questions = [];

        var pairs = this.pairs = this.makePairs();
        console.log("pairs", pairs);

        var msg = {
            round: roundN
        };

        var module = this.roundModule = Round.select(this);
        msg.title = this.moduleTitle = module.getName();
        msg.round = this.round;

        this.sendToAll("round-start", msg);

        this.delay(Times.ROUND_START, this.roundReveal);
    }

    roundReveal() {

        for (var i=0; i<this.players.length; i++) {
            this.players[i].roundData = [];
        }

        this.roundModule.load(function(data) {
            if (data.length != this.pairs.length) {
                console.warn("Invalid lengths " + data.length + ":" + this.pairs.length, data);
                if (this.failures < 4) {
                    this.delay(100, function() {
                        this.failures++;
                        this.roundStart(this.round);
                    });
                }
                else {
                    this.sendToAll("error", {
                        error: "Connection lost.",
                        info: "The game failed to connect to internet services"
                    });
                }
                return;
            }

            this.failures = 0;

            for (var i=0; i<this.pairs.length; i++) {
                var pairI = this.pairs[i];

                var playerA = this.players[i];
                var playerB = this.players[pairI];

                var question = data[i];
                question.round = this.round;
                question.title = this.moduleTitle;
                question.index = i;
                question.playerA = {
                    name: playerA.name,
                    voters: []
                };
                question.playerB = {
                    name: playerB.name,
                    voters: []
                };
                this.questions.push(question);

                playerA.roundData.push(question);
                playerB.roundData.push(question);
            }

            for (var i=0; i<this.players.length; i++) {
                var player = this.players[i];
                player.roundReveal();
            }
            this.client.send("round-reveal", {
                time: 60
            });
        }.bind(this));
    }

    playerAnswered(player, msg) {
        var questionI = msg.index;
        var question = this.questions[questionI];

        var playerNode = null;
        if (question.playerA.name == player.name) {
            playerNode = question.playerA;
        }
        else if (question.playerB.name == player.name) {
            playerNode = question.playerB;
        }

        if (playerNode == null) {
            console.error("No player node");
            return;
        }

        playerNode.answer = msg.answer || "No answer";

        this.checkAnswers();
    }

    checkAnswers() {
        var answered = 0;
        for (var i=0; i<this.questions.length; i++) {
            var question = this.questions[i];
            if (question.playerA.answer && question.playerB.answer) {
                answered++;
            }
        }

        if (answered >= this.questions.length) {
            this.delay(500, this.roundAnswersDone);
        }
    }

    onAnswerTimeUp(msg) {
        this.delay(10, this.roundAnswersDone);
    }

    roundAnswersDone() {
        for (var i=0; i<this.questions.length; i++) {
            var question = this.questions[i];
            if (question.playerA.answer == null) {
                question.playerA.answer = "NO ANSWER";
            }
            if (question.playerB.answer == null) {
                question.playerB.answer = "NO ANSWER";
            }
        }
        this.delay(10, this.voteStart);
    }

    voteStart() {
        this.sendToAll("vote-start");
        this.questionI = -1;
        this.delay(Times.VOTE_START, this.nextVote);
    }

    nextVote() {
        this.questionI++;
        if (this.questionI >= this.questions.length) {
            this.delay(100, this.results);
            return;
        }

        this.voted = [];

        var question = this.questions[this.questionI];
        this.sendToAll("vote", question);
    }

    playerVoted(player, msg) {
        for (var i=0; i<this.voted.length; i++) {
            if (this.voted[i] == player.name) {
                console.log("Duplicate vote.")
                return;
            }
        }

        var questionI = msg.index;
        var question = this.questions[questionI];
        var vote = msg.vote;

        if (vote == 1) {
            question.playerA.voters.push(player.name);
        }
        else if (vote == 2) {
            question.playerB.voters.push(player.name);
        }

        this.voted.push(player.name);

        if (this.voted.length >= this.players.length) {
            this.delay(500, this.tallyVotes);
        }
    }

    onVoteTimeUp() {
        this.delay(10, this.tallyVotes);
    }

    tallyVotes() {
        var question = this.questions[this.questionI];
        question.outcome = 0;

        var pa = question.playerA;
        var pb = question.playerB;

        var tallyPlayer = function(player, otherPlayer) {
            var votes = 0;
            var points = 0;

            for (var i=0; i<player.voters.length; i++) {
                var voter = player.voters[i];

                votes++;

                if (voter.name == player.name) {
                    points += 50;
                    player.pickOwn = true;
                }
                else if (voter.name == otherPlayer.name) {
                    points += 150;
                    player.pickOther = true;
                }
                else {
                    points += 100;
                }
            }

            player.votes = votes;
            player.points = points;
        }

        tallyPlayer(pa, pb);
        tallyPlayer(pb, pa);

        if (pa.points > pb.points) {
            question.winner = 1;
            if (pa.pickOwn) {
                pa.cocksure = true;
            }
            if (pb.pickOther) {
                pb.conceeded = true;
            }
        }
        else if (pb.points > pa.points) {
            question.winner = 2;
            if (pb.pickOwn) {
                pb.cocksure = true;
            }
            if (pa.pickOther) {
                pa.conceeded = true;
            }
        }

        this.getPlayer(pa.name).addPoints(pa.points, pa.votes, pa.conceeded, pa.cocksure);
        this.getPlayer(pb.name).addPoints(pb.points, pb.votes, pb.conceeded, pb.cocksure);

        this.sendToAll("vote-tally", question);
        this.delay(Times.NEXT_VOTE, this.nextVote);
    }

    results() {
        this.sendToAll("results", this.getScores());
        this.delay(Times.RESULTS, function() {
            if (this.round >= this.roundCount) {
                this.sendToAll("end-game", this.getScores());
            }
            else {
                this.round++;
                this.roundStart();
            }
        });
    }

    sendToAll(type, data) {
        for (var i=0; i<this.players.length; i++) {
            var player = this.players[i];
            player.client.send(type, data);
        }
        this.client.send(type, data);
        console.log("send to all", data);
    }

    delay(time, action) {
        clearTimeout(this.timeout);
        if (action) {
            this.timeout = setTimeout(action.bind(this), time);
        }
    }

    getScores() {
        var scores = [];
        for (var i=0; i<this.players.length; i++) {
            var player = this.players[i];
            scores.push({
                name: player.name,
                points: player.points,
                votes: player.votes,
                conceeds: player.conceeds,
                cocksures: player.cocksures
            })
        }
        scores.sort(function(a, b) {
            if (a.points == b.points)
                return 0;
            return a.points > b.points ? -1 : 1;
        })
        return scores;
    }

    getStats() {
        return {
            room: this.name,
            players: this.getPlayers(),
            round: this.getRound()
        };
    }

    getPlayers() {
        var result = [];
        for (var i=0; i<this.players.length; i++) {
            result.push(this.players[i].name);
        }
        return result;
    }

    getPlayer(name) {
        name = name.toUpperCase();
        var i = this.players.length;
        while (i-- > 0) {
            if (this.players[i].name == name)
                return this.players[i];
        }
        return null;
    }

    makePairs() {
        var playerCount = this.players.length;

        if (playerCount == 1) {
            return [0, 0];
        }

        if (playerCount == 2) {
            return [1, 0];
        }

        var pairs = [];

        var indices = [];
        for (var i=0; i<playerCount; i++) {
            indices.push(i);
            pairs.push(-1);
        }
        var count = indices.length;

        for(var i=0; i<count; i++) {
            while (true) {
                var pairI = Math.floor(Math.random() * count);
                if (pairI != i && pairs[pairI] != i) {
                    console.log("pair " + i + " with " + pairI);
                    pairs[i] = pairI;
                    break;
                }
            }
        }

        return pairs;
    }

    getRound() {
        return this.round || 0;
    }

}
