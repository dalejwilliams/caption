/*
CAPTION: A game by Dale J Williams
*/
'use strict';

const http = require('http');
const finalhandler = require('finalhandler');
const serveStatic = require('serve-static');

module.exports = class WebServer {

	constructor(port, staticPath) {
		this.port = port;
		this.running = false;
		this.httpServer = null;
        this.dispatcher = null;
		this.start(staticPath);
	}

	start(staticPath) {
		staticPath = staticPath || "www";

		this.httpServer = http.createServer(this.onRequest.bind(this));
        this.staticServer = serveStatic(staticPath, {
            index: "index.html"
        })

        this.httpServer.listen(this.port, function() {

			console.log(this._connectionKey)

			this.running = true;
			
		}.bind(this));
		console.log("WebServer started on port " + this.port);
	}

    onRequest(request, response) {
        this.staticServer(request, response, finalhandler(request, response));
    }

}
