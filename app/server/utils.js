module.exports = {
    shuffle: shuffle,
    grok: grok,
    grokElem, grokElem,
    blankOneWord: blankOneWord
};

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (currentIndex > 0) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function grok(str, open, close) {
    
    var results = [];

    var parts = str.split(open);
    if (parts.length > 1) {
        for (var i=1; i<parts.length; i++) {
            var part = parts[i];
            var endI = part.indexOf(close);
            if (endI >= 0) {
                part = part.substr(0, endI);
            }
            results.push(part);
        }
    }

    return results;
}

function grokElem(str, open, close) {
    var parts = grok(str, open, close);

    for (var i=0; i<parts.length; i++) {
        var part = parts[i];
        
        part = part.substr(part.indexOf(">"), part.indexOf("<"));

        parts[i] = part;
    }

    return part;
}

function blankOneWord(str) {
    var words = str.split(" ");

    var wordI = -1;
    var attempts = 10;
    while (attempts-- > 0) {
        wordI = Math.floor(Math.random() * words.length);
        if (words[wordI].length >= 5) {
            words[wordI] = "______";
            break;
        }
    }

    return words.join(" ");
}
