const gulp = require('gulp');

const SRC = '.';
const APP = '../builds/win';
const DEST = APP + '/resources';
const WEB = './website';

gulp.task('default', ['build']);

gulp.task('build', ['copy-room', 'copy-player', 'copy-server', 'copy-modules', 'copy-nwjs', 'copy-package'])

gulp.task('copy-room', function() {
   gulp.src(SRC + '/www/**/*.*')
   .pipe(gulp.dest(DEST + '/www'));
});

gulp.task('copy-player', function() {
    gulp.src(SRC + '/www/**/*.*')
   .pipe(gulp.dest(WEB));
});

gulp.task('copy-server', function() {
   gulp.src(SRC + '/server/**/*.*')
   .pipe(gulp.dest(DEST + '/server'));
});

gulp.task('copy-modules', function() {
    gulp.src(SRC + '/node_modules/**/*.*')
   .pipe(gulp.dest(DEST + '/node_modules'));
});

gulp.task('copy-nwjs', function() {
    gulp.src(SRC + '/nwjs/**/*.*')
   .pipe(gulp.dest(DEST));
});

gulp.task('copy-package', function() {
    gulp.src(SRC + '/nwjs/package.json')
   .pipe(gulp.dest(APP));
});