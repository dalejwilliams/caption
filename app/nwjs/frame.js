(function() {

    "use strict";

    const CaptionServer = require("./server/CaptionServer");
    const WebServer = require("./server/WebServer");

    var iframe = null;
    var webServer = null;
    var captionServer = null;
    var loadTimer = null;

    function init() {       
        setTimeout(startServers, 10);
    }

    function startServers() {
        captionServer = new CaptionServer(26666);
        webServer = new WebServer(80, "resources/www");

        //loadWhenReady();

        setTimeout(startFrame, 1000);
    }

    function loadWhenReady() {    
        clearTimeout(loadTimer);
        loadTimer = setTimeout(function() {
            clearTimeout(loadTimer);
            if (webServer.running) {
                startFrame();
            }
        }, 500);
    }

    function startFrame() {
        if (iframe == null) {
            iframe = document.createElement("iframe");            
            document.body.appendChild(iframe);
        }
        iframe.src = "www/room/index.html";
    }

    window.addEventListener("load", init);

})();