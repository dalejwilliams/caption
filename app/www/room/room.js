// (c) copyright 2016 Dale J Williams
(function() {

    'use strict';

    var room = window.room = {
        init: init,
        onConnect: onConnect,
        onMessage: onMessage
    };

    var handlers = {
        "auth":         onAuth,
        "players":      onPlayers,
        "game-start":   onGameStart,
        "round-start":  onRoundStart,
        "round-reveal": onRoundReveal,
        "vote-start":   onVoteStart,
        "vote":         onVote,
        "vote-tally":   onVoteTally,
        "results":      onResults
    };

    var data = null;
    var container = null;
    var initW = 1280;
    var initH = 720;
    var timer = {};

    function init() {
        app.init();
        data = app.data;
        container = document.getElementById("container");
        initW = container.offsetWidth;
        initH = container.offsetHeight;
        sock.handler = room;
        sock.connect();
        resize();
        addMusic();
    }

    function addMusic() {
        sound.addLoop({
            src: "/content/music/round.ogg",
            start: 0.626,
            end: 16.614
        });
         sound.addLoop({
            src: "/content/music/round_esc.ogg",
            start: 0.626,
            end: 16.614
        });
    }

    function resize() {
        var targetW = initW;
        var targetH = initH;

        var winW = window.innerWidth;
        var winH = window.innerHeight;

        var rw = winW/targetW;
        var rh = winH/targetH;

        var scale = rw < rh ? rw : rh;
        container.style.transform = "scale(" + scale + ")";
    }

    function onConnect() {
        sock.send("room-auth");
    }

    function onMessage(type, data) {
        console.log("host message", type, data);

        var handler = handlers[type];
        if (handler != null) {
            handler(data);
        }
        else {
            console.log("No handler for " + type);
        }
    }

    function onAuth(msg) {
        data.room = msg.room;
        data.ip = msg.ip;
        data.url = msg.url;
        app.goto("splash");
        setTimeout(function() {
            app.goto("lobby");
        }, 2000);
    }

    function onPlayers(msg) {
        var players = data.players = msg.players;
        data.playerCount = players.length;

        var html = "";
        for (var i=0; i<players.length; i++) {
            html += "<li>" + players[i] + "</li>";
        }
        utils.fillClasses("players", html);
    }

    function onGameStart(msg) {
        app.goto("game-start");
    }

    function onRoundStart(msg) {
        data.round = {
            round: msg.round,
            title: msg.title
        };
        app.goto("round-start");
        utils.fillClasses("time", "60s");
    }

    function onRoundReveal(msg) {
        app.goto("round");
        //timerStart(msg.time, roundTimerEnd);
        setTimeout(function() {

            var timers = app.getModules("Countdown");
            if (timers.length > 0) {
                timers[0].startTimer(msg.time, roundTimerEnd);
            }

        }, 1000);
    }

    function onVoteStart(msg) {
        app.goto("vote-start");
        timerStop();
    }

    function onVote(msg) {
        data.vote = msg;
        app.goto("vote");
        timerStop();
    }

    function onVoteTally(msg) {
        var showTally = function(players, elem) {
            var html = "";
            for (var i=0; i<players.length; i++) {
                html += "<li>" + players[i] + "</li>";
            }
            utils.fillClasses(elem, html);
        }

        setTimeout(function() {
            showTally(msg.playerA.voters, "votesA");
            setTimeout(function() {
                showTally(msg.playerB.voters, "votesB");
            }, 300);
        }, 100);
    }

    function onResults(msg) {
        app.goto("results");

        setTimeout(function(){
            var html = "";
            for (var i=0; i<msg.length; i++) {
                var score = msg[i];
                html += '<li>';
                html += '<span class="name">' + score.name + ':</span> ';
                html += '<span class="score">' + score.points + '</span>';
                html += '</li>';
            }
            utils.fillClasses("results", html);
        }, 1000);
    }

    function roundTimerEnd() {
        sock.send("answer-time-up");
    }

    function timerStart(seconds, onTimerEnd) {
        timer.startTime = new Date().getTime();
        timer.duration = seconds * 1000;
        timer.onEnd = onTimerEnd;
        events.onUpdate.add(timerUpdate);
    }

    function timerUpdate() {
        timer.now = new Date().getTime();
        timer.lapsed = timer.now - timer.startTime;
        timer.left = timer.duration - timer.lapsed;

        var secondsLeft = Math.ceil(left / 1000);
        utils.fillClasses("time", secondsLeft + "s");

        if (timer.left <= 0) {
            timerStop();
            if (timer.onEnd) timer.onEnd();
        }
    }

    function timerStop() {
        utils.fillClasses("time", "DONE!");
        events.onUpdate.remove(timerUpdate);
        timer = {};
    }

    window.addEventListener("load", init);
    window.addEventListener("resize", resize);

})();
