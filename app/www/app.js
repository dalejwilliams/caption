// (c) copyright 2016 Dale J Williams
(function() {

    'use strict';

    var app = window.app = {
        init: init,
        goto: goto,
        login: login,
        alert: alert,
        prompt: prompt,
        save: save,
        data: {
            host: "127.0.0.1",
            save: {}
        },
        debug: debug,
        setModule: setModule,
        getModule: getModule,
        getModules: getModules
    };

    var mainViews = [];
    var viewCurrent = null;
    var viewPrevious = null;
    var viewIndex = 0;
    var alertBox = null;
    var alertConfirm = null;
    var data = app.data;
    var animTimeouts = [];
    var modules = {};
    var moduleInstances = [];
    var buttonListeners = [];
    var time = 0;

    function init() {
        mainViews = document.getElementsByClassName("main-view");
        alertBox = document.getElementById("alert-box");
        sound.addGroup("music", 0.5);
        load();
        events.onUpdate.add(update);
        events.onKey.add(keyPress);
    }

    function update() {
        var now = new Date().getTime();
        var dt = (now - time) / 1000;
        time = now;

        for (var i=0; i<moduleInstances.length; i++) {
            var module = moduleInstances[i];

            if (!module.started) {
                if (typeof module.start === "function") {
                    module.start();
                }
                module.started = true;
            }
            else {
                if (typeof module.update === "function") {
                    module.update(dt);
                }
            }
        }
    }

    function keyPress(key) {
        // escape
        if (key == 27) {
            console.log("close");
            window.close();
            if (typeof win !== undefined) {
                win.close(true);
            }
        }
    }
    
    function setModule(classFunc) {
        if (typeof classFunc !== "function") {
            throw new Error("module must be a type function");
        }
        
        var moduleName = classFunc.name;
        modules[moduleName] = classFunc;
        console.log("added module " + moduleName);
    }

    function getModule(moduleName) {
        return modules[moduleName] || null;
    }

    function getModules(moduleName) {
        var modules = [];

        for (var i=0; i<moduleInstances.length; i++) {
            var module = moduleInstances[i];
            if (module.name == moduleName) {
                modules.push(module);
            }
        }

        return modules;
    }

    function goto(section) {
        var url = "view/" + section + ".html";
        var view = getNextView();
        setAnim(view, "off");
        utils.loadHtml(url, view, app.data, onPageLoad);
    }

    function getNextView() {
        if (viewCurrent != null) {
            viewPrevious = viewCurrent;
        }
        viewCurrent = mainViews[viewIndex];
        viewIndex++;
        if (viewIndex >= mainViews.length) {
            viewIndex = 0;
        }
        return viewCurrent;
    }

    function onPageLoad() {
        utils.latchForms();

        if (viewPrevious) {
            setAnim(viewPrevious, "out");
        }
        setAnim(viewCurrent, "in");

        var animElems = viewCurrent.querySelectorAll(".anim");
        for (var i=0; i<animElems.length; i++) {
            var animElem = animElems[i];
            setAnim(animElem, "off");
            setAnim(animElem, "in", i * 300 + 600);
        }

        var musicElem = viewCurrent.querySelector("[music]");
        if (musicElem) {
            var src = "/content/music/" + musicElem.getAttribute("music");
            sound.play(src, "music");
        }

        latchModules();
    }

    function latchModules() {
        if (viewPrevious) {
            var oldModuleElems = viewPrevious.querySelectorAll("[module]");
            for (var i=0; i<oldModuleElems.length; i++) {
                var moduleElem = oldModuleElems[i];
                if (moduleElem.module) {
                    for (var j=0; j<moduleInstances.length; j++) {
                        if (moduleInstances[i] != moduleElem.module) continue;
                        moduleInstances.splice(i, 1);
                    }
                }
            }
        }

        setTimeout(function() {

            var moduleElems = document.querySelectorAll("[module]");

            for (var i=0; i<moduleElems.length; i++) {
                var moduleElem = moduleElems[i];
                if (moduleElem.module === undefined) {
                    var moduleName = moduleElem.getAttribute("module");
                    if (moduleName) {
                        var moduleClass = getModule(moduleName);
                        if (moduleClass != null) {
                            var module = new moduleClass(moduleElem);
                            module.name = moduleName;
                            console.log("Latch module " + moduleName);
                            moduleInstances.push(module);
                            moduleElem.module = module;
                        }
                    }
                }
            }

        }, 10);
    }

    function setAnim(elem, state, delay) {
        if (isNaN(delay) == false && delay > 0) {
            var timeout = setTimeout(function() {
                setAnim(elem, state);
            }, delay);
            animTimeouts.push(timeout);
            return;
        }
        else {
            console.log("<" + elem.tagName + "." + elem.className.replace(" ", "    .") + "> anim " + state);
            elem.classList.remove("off");
            elem.classList.remove("out");
            elem.classList.remove("in");
            elem.classList.add(state);
        }
    }

    function login(data) {
        client.connect(data.room, data.name);
    }

    function alert(message, onConfirm) {
        var html = '<p>' + message + '</p>';
        html += '<button onclick="app.alertClose()">Sure!</button>';
        alertBox.innerHTML = html;
        alertBox.classList.add("show");
        alertConfirm = onConfirm;
    }

    function prompt(message, onConfirm) {
        var html = '<p>' + message + '</p>';
        html += '<button onclick="app.alertClose()">Yep</button>';
        html += '<button onclick="app.alertClose(true)">Nup</button>';
        alertBox.innerHTML = html;
        alertBox.classList.add("show");
        alertConfirm = onConfirm;
    }

    function alertClose(cancel) {
        alertBox.classList.remove("show");
        if (alertConfirm && cancel !== true) {
            setTimeout(function() {
                alertConfirm();
            }, 100);
        }
        alertConfirm = null;
    }

    function save(newInfo) {
        app.data.save = utils.mixin(app.data.save || {}, newInfo);
        var str = utils.objToJson(app.data.save);
        window.localStorage.setItem("save", str);
    }

    function load() {
        var str = window.localStorage.getItem("save") || "{}";
        var obj = utils.jsonToObj(str);
        app.data.save = obj;
    }

    function debug(section) {
        var data = {
            round: 1,
            playerA: {
                answer: "ANSWER ONE"
            },
            playerB: {
                answer: "ANSWER TWO"
            }
        };

        if (Math.random() < 0.5) {
            data.text = "_____ is a question of sorts.";
        }
        else {
            data.image = "/content/bitmap.png";
        }

        app.data.vote = data;

        goto(section);
    }

})();