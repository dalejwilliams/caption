// (c) copyright 2016 Dale J Williams
(function() {

    "use strict";

    app.setModule(Countdown);

    function Countdown(element) {
        this.element = element;
        this.counting = false;
        this.duration = 0;
        this.left = 0;
        this.onTimerDone = null;

        this.countElem = document.createElement("div");
        this.countElem.className = "counter";
        this.element.appendChild(this.countElem);
    }

    var p = Countdown.prototype;

    p.update = function(dt) {
        if (this.counting == false) return;

        var now = new Date().getTime();

        this.elapsed = now - this.startTime;
        this.left = (this.duration * 1000) - this.elapsed;

        this.secondsLeft = Math.floor(this.left / 1000);
        this.countElem.innerText = Math.max(0, this.secondsLeft);

        if (this.left <= 0) {
            this.counting = false;
            this.element.classList.add("out");
            if (this.onTimerDone != null) {
                this.onTimerDone();
            }
        }
    };

    p.startTimer = function(duration, onTimerDone) {
        this.duration = duration;
        this.left = duration;
        this.startTime = new Date().getTime(); 
        this.counting = true;
        this.element.classList.add("in");
        this.onTimerDone = onTimerDone;
    };

})();