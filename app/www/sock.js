// (c) copyright 2016 Dale J Williams
(function() {

    'use strict';

    var sock = window.sock = {
        connect: connect,
        send: send,
        handler: null
    };

    var ws = null;

    function connect() {
        ws = new WebSocket("ws://" + app.data.host + ":26666");
        ws.onopen = function(e) {
            sock.handler.onConnect(e);
        };
        ws.onmessage = function(e) {
            onMessage(e.data);
        };
    }

    function send(type, data) {
        var msg = {
            type: type
        };
        if (data) {
            msg.data = data;
        }
        var str = utils.objToJson(msg);
        console.log("send", str);
        ws.send(str);
    }

    function onMessage(str) {
        console.log("receive", str);
        var obj = utils.jsonToObj(str);
        if (obj == null) {
            console.error("Invalid message", str);
            return;
        }
        var type = obj.type;
        var data = obj.data;
        sock.handler.onMessage(type, data);
    }

})();
