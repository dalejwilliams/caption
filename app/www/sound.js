// (c) copyright 2016 Dale J Williams
(function() {

    'use strict';

    var sound = window.sound = {
        DEFAULT_NAME: "default",
        play: play,
        stop: stop,
        preload: preload,
        stopAll: stopAll,
        addGroup: addGroup,
        addLoop: addLoop,
        testLoops: testLoops
    };

    var groups = sound.groups = {};
    var audios = sound.audios = {};
    var loops = sound.loops = {};
    var host = null;

    function init() {
        host = document.createElement("div");
        host.id = "audio";
        document.body.appendChild(host);

        addGroup(sound.DEFAULT_NAME);
    }

    function preload(file, name) {
        if (file === undefined) throw new Error("Must specify file.");
        name = name || file;

        if (audios[name]) return;

        var audio = createAudio(file, volume);
        audios[name] = audio;
    }

    function addLoop(loop) {
        if (!loop.src) throw new Error("loop needs a src");
        if (!loop.start) throw new Error("loop needs a start");
        if (!loop.end) throw new Error("loop needs an end");
        loops[loop.src] = loop;
    }

    function play(name, groupName) {
        if (name === undefined) throw new Error("Must specify name.");
        groupName = groupName || sound.DEFAULT_NAME;

        var group = groups[groupName];
        if (group == null) {
            group = addGroup(groupName);
        }

        var src = name;
        var loopStart = 0;
        var loopEnd = 0;

        var loop = loops[name];
        if (loop) {
            src = loop.src;
            loopStart = loop.start;
            loopEnd = loop.end;
        }
        else {
            var audio = audios[name];
            if (audio != null) {
                src = audio.src;
            }
        }

        src = utils.contentPath(src);

        var element = group.elements[group.elementI];
        if (src == element.src) return;

        element.pause();
        element.src = src;
        element.loopStart = loopStart;
        element.loopEnd = loopEnd;
        element.play();
        group.current = element;
        group.elementI++;
        if (group.elementI >= group.elements.length) {
            group.elementI = 0;
        }

        console.log("playing " + name + " on " + groupName + " " + (loopEnd > 0 ? "looping" : "once"));
    }

    function stop(groupName) {
        if (groupName === undefined) throw new Error("Must specify groupName.");

        var group = groups[groupName];
        if (group == null) return;screen

        for (var i=0; i<group.elements.length; i++) {
            var element = group.elements[i];
            element.stop();
        }

        group.elementI = 0;
    }

    function stopAll() {
        for (var key in groups) {
            stop(key);
        }
    }

    function addGroup(name, volume, elementCount) {
        if (name === undefined) throw new Error("Must specify name.");

        volume = volume || 1;
        elementCount = elementCount || 1;

        var group = {
            name: name,
            volume: volume,
            elements: [],
            elementI: 0,
            current: null
        };

        for (var i=0; i<elementCount; i++) {
            var element = createAudio(name, volume);
            group.elements.push(element);
        }

        sound.groups[name] = group;

        return group;
    }

    function createAudio(name, volume) {
        volume = volume || 1;
        var audio = new Audio();
        audio.src = utils.contentPath(name);
        audio.controls = false;
        audio.autoplay = false;
        audio.volume = volume;
        audio.preload = true;
        return audio;
    }

    function monitor() {
        for (var key in groups) {
            var group = groups[key];

            var sound = group.current;
            if (!sound || sound.loopEnd <= 0) continue;

            var over = sound.currentTime - sound.loopEnd;
            if (over > 0) {
                var loopLength = sound.loopEnd - sound.loopStart;
                sound.currentTime -= loopLength;
            }
        }
    }

    function testLoops() {
        for (var key in groups) {
            var group = groups[key];

            var sound = group.current;
            if (!sound || sound.loopEnd <= 0) continue;

            sound.currentTime = sound.loopEnd - 2;
        }
    }

    window.addEventListener("load", init);
    setInterval(monitor, 50);

})();